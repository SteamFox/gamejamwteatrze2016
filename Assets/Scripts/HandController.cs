﻿using UnityEngine;
using System.Collections;

public class HandController : MonoBehaviour {
	public const string HorizontalAxis = "HandHorizontal";
	public const string VerticalAxis = "HandVertical";
	public const string PushAxis = "HandPush";
	public GameObject handPrefab;
	public bool locked;
	public GameObject ps;

	public void Awake() {
		locked = false;
	}

	public void Update() {
		float ax = Input.GetAxisRaw(HorizontalAxis);
		float ay = Input.GetAxisRaw(VerticalAxis);
		Vector2 side = new Vector2(ax, ay);
		if(side != Vector2.zero) {
			MoveHandTo(side);
		}
		float ap = Input.GetAxisRaw(PushAxis);
		if(ap != 0) {
			PushNearby();
		}
	}

	public void PushNearby() {
		if(!locked && Game.Me.IsBeat) {
			ps.SetActive(true);
			var emission = ps.GetComponent<ParticleSystem>().emission;
			emission.rate = 3000;
			ps.GetComponent<AudioSource>().Play();
			Vector2 point = transform.position;
			float force = 10;
			Collider2D[] hits = Physics2D.OverlapCircleAll(transform.position, 2.0f);
			foreach(Collider2D col in hits) {
				if(!col.attachedRigidbody.isKinematic && col.tag != Game.Interactable && col.transform.position.y >= transform.position.y) {
					float dist = Vector2.Distance(col.transform.position, point);
					Vector2 forceV = ((new Vector2(col.transform.position.x, col.transform.position.y) - point) * force) / dist;
					col.gameObject.GetComponent<Rigidbody2D>().AddForce(forceV, ForceMode2D.Impulse);
					StartCoroutine(Game.Me.AddForce(col.gameObject.GetComponent<Rigidbody2D>(), -forceV, 1f));
				}
			}
			locked = true;
			StartCoroutine(Unlock());
			StartCoroutine(ResetFlakes());
		}
	}

	public void MoveHandTo(Vector2 side) {
		if(!locked) {
			GetComponent<AudioSource>().Play();
			side.Normalize();
			Collider2D[] colliders = Physics2D.OverlapCircleAll(new Vector2(transform.position.x, transform.position.y) + new Vector2(side.x, side.y), 0.1f);
			if(colliders == null || colliders.Length == 0) {
				return; // NO HIT
			}
			Collider2D col = colliders[0];
			if(col.gameObject.tag == Game.Interactable || col.GetComponent<TileComponent>().CanGoThrough()) {
				transform.position = new Vector3(col.transform.position.x, col.transform.position.y, -1);
			}
			locked = true;
			StartCoroutine(Unlock());
		}
	}

	public IEnumerator Unlock() {
		yield return new WaitForSeconds(0.1f);
		locked = false;
	}

	public IEnumerator NormalizeJumpForce(DemoScene sc) {
		yield return new WaitForSeconds(1f);
		sc.jumpForce = 1.0f;
	}

	private IEnumerator ResetFlakes() {
		yield return new WaitForSeconds(0.3f);
		var emission = ps.GetComponent<ParticleSystem>().emission;
		emission.rate = 0;
	}
}
