﻿public enum TileType {
	Ground = 1,
	Exit = 2,
	Entrance = 3,
	KinematicGround = 4,
	Torch = 7,
	HandStart = 8,
	Phazed = 9,
	GroundNoPass = 11,
	GroundNoPassDisabled = 12
}