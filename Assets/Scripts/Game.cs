﻿using UnityEngine;
using System.Linq;
using Prime31;
using System;
using System.Collections;
using UnityEngine.UI;

public class Game : MonoBehaviour {

	public float JumpHeight;

	public GameObject TilePrefab;
	public Transform Level;
	public SongConf SongConf;
	public GameObject Player;
	public GameObject Camera;
	private float FinishedLast;
	private GameData GameData;
	private int ActualLevel;
	private GameObject SorcerersHand;
	public Sprite[] TileSprites;
	public GameObject EmissionRate;
	public AudioClip HandClip;
	public AudioClip HandPowerClip;

	public bool IsBeat { get; private set; }

	public float LastBeatEndTime;
	public static Game Me;

	public const string Interactable = "Interactable";

	public RuntimeAnimatorController kidsAnimator, badmanAnimator, ladyAnimator, demonAnimator;

	public void Awake() {
		if(Me != null) {
			Destroy(gameObject);
		}
		Me = this;
		GameData = JsonUtility.FromJson<GameData>(Resources.Load<TextAsset>("game").text);
		LoadLevel();
	}

	private void onTriggerEnter2D(Collider2D col) {
		if(col.GetComponent<TileComponent>() != null && col.GetComponent<TileComponent>().TileType == TileType.Exit && Time.time - 1f > FinishedLast) {
			//we don't want more thank you
			FinishedLast = Time.time;
			LevelFinished();
		}
	}

	private void LevelFinished() {
		ActualLevel++;
		if(GameData.levels.Length <= ActualLevel) {
			throw new Exception("End game.");
		}
		LoadLevel();
	}

	private void LoadLevel() {

		foreach(Transform t in Level.transform) {
			Destroy(t.gameObject);
		}

		LevelConfig levelConfig = GameData.levels[ActualLevel];
		InitLevel(levelConfig.init);
		StartCoroutine(PlotTwist(levelConfig, 0));
	}

	private void InitLevel(Init init = null) {
		GameObject.Find("jebutnieWIelkiTekst").GetComponent<Text>().text = init.text;
		GameObject.Find("jebutnieWIelkiTekst").GetComponent<Text>().color = Color.white;
		GameObject.Find("jebutnieWIelkiObrazek").GetComponent<Image>().color = new Color(0, 0, 0, 0);
		GameObject particles = GameObject.Find("particles");
		foreach(Transform p in particles.transform) {
			p.gameObject.SetActive(false);
		}
		if(init.flakes != null) {
			Transform fnd = particles.transform.FindChild(init.flakes);
			if(fnd != null) {
				fnd.gameObject.SetActive(true);
			} else {
				throw new Exception("Unknown flakes!");
			}
		}
		SetupCharacter(init.character);
	}

	private void SetupCharacter(string character) {
		switch(character) {
			case "kids":
				GameObject.Find("Player").GetComponent<Animator>().runtimeAnimatorController = kidsAnimator;
				break;
			case "badman":
				GameObject.Find("Player").GetComponent<Animator>().runtimeAnimatorController = badmanAnimator;
				break;
			case "lady":
				GameObject.Find("Player").GetComponent<Animator>().runtimeAnimatorController = ladyAnimator;
				break;
			case "demon":
				GameObject.Find("Player").GetComponent<Animator>().runtimeAnimatorController = demonAnimator;
				break;
			default:
				goto case "kids";
		}
	}

	private IEnumerator PlotTwist(LevelConfig config, int step, float wait = 0.0f) {
		yield return new WaitForSeconds(wait);
		if(config.plot == null || step >= config.plot.Length) {
			FinishLoading(config);
			yield return null;
		} else {
			Plot plot = config.plot[step];
			switch(config.plot[step].function) {
				case "fade_to_black":
					StartCoroutine(FadeTo(plot.time, Color.black));
					break;
				case "fade_to_transparent":
					StartCoroutine(FadeTo(plot.time, new Color(1, 1, 1, 0)));
					break;
				case "play_song":
					StartCoroutine(LoadSongAsync(plot.path));
					break;
				case "wait":
				// Nothing, recursion takes care of all
					break;
				case "showText":
					StartCoroutine(ShowText(plot.text));
					break;
				default:
					throw new Exception("Unknown function name!");
			}
			yield return PlotTwist(config, ++step, plot.time);
		}
	}

	public void FinishLoading(LevelConfig levelConfig) {
		Level level = JsonUtility.FromJson<Level>(Resources.Load<TextAsset>(levelConfig.mapPath).text);
		CreateBorders(level);
		CreateLevel(level);
		//for game jams sake - let's not make it harder than it has to be. our tiles are 1x1 in size, so the bounds of scene
		//starting at 0,0 are level.width, level.height
		Camera.GetComponent<SmoothFollow>().maxX = level.width;
		Camera.GetComponent<SmoothFollow>().maxY = level.height;
		Camera.GetComponent<Camera>().orthographicSize = 11;

		Player.GetComponent<CharacterController2D>().onTriggerEnterEvent += onTriggerEnter2D;

		LoadSong(levelConfig.songConfPath);
	}

	public IEnumerator FadeTo(float time, Color c) {
		GameObject jwo = GameObject.Find("jebutnieWIelkiObrazek");
		Image jwoI = jwo.GetComponent<Image>();
		Color f = jwoI.color;
		float passed = 0;
		for(int i = 0; i < 255; i++) {
			
			jwoI.color = Color.Lerp(jwoI.color, c, passed / time);
			passed += time / 255;

			yield return new WaitForSeconds(time / 255);
		}
	}

	public IEnumerator ShowText(string text) {
		Text t = GameObject.Find("jebutnieWIelkiTekst").GetComponent<Text>();
		t.color = Color.white;
		t.text = text;
		yield return null;
	}

	private void LoadSong(string songConfigPath) {
		if(songConfigPath == null) throw new Exception("Song Config Path is null!");
		TextAsset ta = Resources.Load<TextAsset>(songConfigPath);
		if(ta == null) return;
		SongConf songConf = JsonUtility.FromJson<SongConf>(ta.text);
		GetComponent<AudioSource>().clip = Resources.Load<AudioClip>(songConf.songName);
		GetComponent<AudioSource>().Play();
		SongConf = songConf;
	}

	private IEnumerator LoadSongAsync(string songConfigPath) {
		yield return null;
		LoadSong(songConfigPath);
	}

	private void CreateBorders(Level level) {

		for(int x = -1; x <= level.width; x++) {
			GameObject gameTile = Instantiate(TilePrefab);
			gameTile.GetComponent<Rigidbody2D>().isKinematic = true;
			gameTile.transform.SetParent(Level);
			gameTile.transform.localPosition = new Vector3(x, -1);
		}
	}

	private void CreateLevel(Level level) {
		TilePrefab.SetActive(true);
		TileSprites = Resources.LoadAll<Sprite>("levels/tiles");

		//show physical layer
		Layer physicalLayer = level.layers.FirstOrDefault(t => t.name == LayerType.Physical.Name);
		for(int x = 0; x < level.width; x++) {
			for(int y = 0; y < level.height; y++) {

				int tileId = physicalLayer.data[y * level.width + x];
				if(tileId != 0) {
					GameObject gameTile = Instantiate(TilePrefab);
					gameTile.GetComponent<TileComponent>().TileType = (TileType)tileId;
					gameTile.transform.SetParent(Level);
					gameTile.transform.localPosition = new Vector3(x, level.height - y - 1);
					gameTile.GetComponent<SpriteRenderer>().sprite = TileSprites[tileId - 1];
					if(tileId == (int)TileType.Torch) {
						gameTile.GetComponent<BoxCollider2D>().enabled = false;
						gameTile.GetComponent<Rigidbody2D>().isKinematic = true;
					}
					if(tileId == (int)TileType.KinematicGround) {
						gameTile.GetComponent<Rigidbody2D>().isKinematic = true;
						gameTile.tag = Interactable;
					}
					if(tileId == (int)TileType.Phazed) {
						gameTile.GetComponent<Rigidbody2D>().isKinematic = true;
					}
					if(tileId == (int)TileType.GroundNoPass) {
						gameTile.GetComponent<Rigidbody2D>().isKinematic = true;
					}
				}
			}
		}

		Layer functionsLayer = level.layers.FirstOrDefault(t => t.name == "functions");
		//functional layer -> where player starts
		for(int x = 0; x < level.width; x++) {
			for(int y = 0; y < level.height; y++) {
				int tileId = functionsLayer.data[y * level.width + x];
				if(tileId != 0) {
					if(tileId == (int)(TileType.Entrance)) {
						Player.transform.localPosition = new Vector3(x, level.height - y - 1);
					}

					if(tileId == (int)(TileType.HandStart)) {
						GameObject gameTile = Instantiate(TilePrefab);
						gameTile.name = "Hand";
						SorcerersHand = gameTile;
						gameTile.transform.SetParent(Level);
						gameTile.transform.localPosition = new Vector3(x, level.height - y - 1, -1);
						gameTile.GetComponent<SpriteRenderer>().sprite = TileSprites[tileId - 1];
						GameObject go = Instantiate(EmissionRate);
						go.transform.SetParent(gameTile.transform);
						go.transform.localPosition = Vector2.up;
						go.SetActive(false);
						gameTile.AddComponent<HandController>().ps = go;
						gameTile.AddComponent<AudioSource>().clip = Game.Me.HandClip;
						gameTile.GetComponent<AudioSource>().volume = 0.30f;
						go.AddComponent<AudioSource>().clip = Game.Me.HandPowerClip;
						go.GetComponent<AudioSource>().volume = 0.30f;

						Destroy(gameTile.GetComponent<Collider2D>());
						Destroy(gameTile.GetComponent<Rigidbody2D>());
					}
					if(tileId == (int)(TileType.Exit)) {
						GameObject gameTile = Instantiate(TilePrefab);
						gameTile.transform.SetParent(Level);
						gameTile.transform.localPosition = new Vector3(x, level.height - y - 1);
						gameTile.GetComponent<SpriteRenderer>().sprite = TileSprites[tileId - 1];
						gameTile.GetComponent<Rigidbody2D>().isKinematic = true;
						gameTile.GetComponent<BoxCollider2D>().isTrigger = true;
						gameTile.GetComponent<TileComponent>().TileType = TileType.Exit;
						gameTile.layer = LayerMask.NameToLayer("Triggers");
					}
				}
			}
		}
		TilePrefab.SetActive(false);
		if(SorcerersHand == null) {
			throw new Exception("Sorcerers Hand has no set place!");
		}
	}

	public class LayerType {
		public static readonly LayerType Physical = new LayerType("physical");

		public readonly string Name;

		public LayerType(string name) {
			Name = name;
		}
	}

	void Update() {

		//show beats
		AudioSource asource = GetComponent<AudioSource>();
		float offset = asource.time % SongConf.rate;
		//Debug.Log("song time: " + asource.time + ", actualOffset: "+ offset + " deltaTime: " + Time.deltaTime + " , rate: " + SongConf.rate + " beat: " + SongConf.beats[0]);

		IsBeat = false;
		foreach(float beat in SongConf.beats) {
			float powerStrength = Mathf.Abs(offset - beat);
			if(powerStrength < 0.2 && !IsBeat) {
				IsBeat = true;
				LastBeatEndTime = 0;
			}
		}
		Vector3 pos = GameObject.Find("dit").transform.localPosition;
		if(SongConf.rate != 0) {
			GameObject.Find("dit").transform.localPosition = new Vector2(((offset / SongConf.rate) * 200) % 100 - 100, pos.y);
			GameObject.Find("dit2").transform.localPosition = new Vector2(-((offset / SongConf.rate) * 200) % 100 + 100, pos.y);
		}

		if(!IsBeat) {
			LastBeatEndTime += Time.deltaTime;
		}
		GameObject.Find("ring").transform.localScale = IsBeat ? Vector3.one * 3 : Vector3.one * 2;
		Player.GetComponent<DemoScene>().jumpHeight = JumpHeight * (IsBeat ? 2 : 1);
	}

	public static Collider2D[] GetCollidersAround(Vector2 point, float distance) {
		return Physics2D.OverlapCircleAll(point, distance);
	}

	private void WorldTouchedAt(Vector2 point, float force) {

		Collider2D[] colliders = GetCollidersAround(point, 2);
		foreach(Collider2D col in colliders) {
			float dist = Vector2.Distance((Vector2)col.gameObject.transform.position, point);
			Vector2 forceV = ((Vector2)col.gameObject.transform.position - point) * force * 1 / dist;
			col.gameObject.GetComponent<Rigidbody2D>().AddForce(forceV, ForceMode2D.Impulse);
			if(col.gameObject.GetComponent<CharacterController2D>() != null) {
				StartCoroutine(AddForce(col.gameObject.GetComponent<Rigidbody2D>(), -forceV, 1f));
			}
		}
	}

	public IEnumerator AddForce(Rigidbody2D r, Vector2 f, float time) {
		yield return new WaitForSeconds(time);
		r.velocity = Vector2.zero;
		r.angularVelocity = 0;
		yield return null;
	}

}
