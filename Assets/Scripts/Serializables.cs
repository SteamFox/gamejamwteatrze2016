﻿using System;
using UnityEngine;

[Serializable]
public class GameData {
	public LevelConfig[] levels;
}

[Serializable]
public class LevelConfig {
	public string mapPath;
	public string songConfPath;
	public Plot[] plot;
	public Init init = new Init();
}

[Serializable]
public class Init {
	public string text = "";
	public string flakes;
	public string character;
}

[Serializable]
public class Plot {
	public string function;
	public float time;
	public string path;
	public string text;
}

[Serializable]
public class Level {
	public int height;
	public int width;
	public Layer[] layers;
	public Tileset[] tilesets;
}

[Serializable]
public class Layer {
	public int[] data;
	public string name;
}


[Serializable]
public class Tileset {
	public int columns;
	public int firstgit;
	public string image;

}

[Serializable]
public class SongConf {
	public string songName;
	public float rate;
	public float[] beats;
}