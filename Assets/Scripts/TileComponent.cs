﻿
using UnityEngine;

public class TileComponent : MonoBehaviour {
	public TileType TileType;

	public bool IsDisabled() {
		Collider2D[] colliders = Game.GetCollidersAround(transform.position, 2f);
		foreach (Collider2D col in colliders) {
			if (col.GetComponent<NoPassDisabler>() != null) {
				return true;
			}
		}
		return false;
	}

	public bool IsTransparentFX {
		get {
			return gameObject.layer == LayerMask.NameToLayer("TransparentFX");
		}
		set {
			gameObject.layer = LayerMask.NameToLayer(value ? "TransparentFX" : "Default");
			GetComponent<SpriteRenderer>().color = value ? new Color(1, 1, 1, 0.5f) : new Color(1, 1, 1, 1f);
		}
	}

	public float TurnedOffTime;

	internal bool CanGoThrough() {
		return IsDisabled() || TileType == TileType.Ground || TileType == TileType.Phazed;
	}

	void Update() {

		if (TileType == TileType.GroundNoPass) {
			bool isDisabled = IsDisabled();
			GetComponent<SpriteRenderer>().sprite = Game.Me.TileSprites[(isDisabled ? (int)TileType.GroundNoPassDisabled : (int)TileType.GroundNoPass) - 1];
			GetComponent<SpriteRenderer>().color = isDisabled  ? Color.gray : Color.white; //not asking IsDisabled() because of optimalization 
		}

		if (TileType == TileType.Phazed) {
			if (IsTransparentFX) {
				TurnedOffTime += Time.deltaTime;

				if (TurnedOffTime > 0.5) {
					IsTransparentFX = false;
				}
			}

			if (Game.Me.LastBeatEndTime > 0.1 && Game.Me.LastBeatEndTime - Time.deltaTime < 0.1) {
				IsTransparentFX = true;
				TurnedOffTime = 0;
			}

		}
	}
}
