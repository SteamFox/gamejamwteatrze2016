using UnityEngine;
using Prime31;

public class SmoothFollow : MonoBehaviour
{
	public Transform target;
	public float smoothDampTime = 0.2f;
	[HideInInspector]
	public new Transform transform;
	public Vector3 cameraOffset;
	public float maxX;
	public float maxY;
	
	private CharacterController2D _playerController;
	private Vector3 _smoothDampVelocity;

	void Awake()
	{
		transform = gameObject.transform;
		_playerController = target.GetComponent<CharacterController2D>();
	}
	
	private Vector3 clampTargetPos(Vector3 targetPos) {
		float vertExtent = GetComponent<Camera>().orthographicSize;
		float horzExtent = vertExtent * Screen.width / Screen.height;

		float minX = -0.5f + horzExtent;
		float maxX = this.maxX - 0.5f - horzExtent;

		float minY = -0.5f + vertExtent;
		float maxY = this.maxY + 0.5f - vertExtent;

		return new Vector3(Mathf.Clamp(targetPos.x, minX, maxX), Mathf.Clamp(targetPos.y, minY, maxY));
	}

	private void UpdateCameraPosition() {

		//update target position
		Vector3 targetPos = clampTargetPos(target.position);

		if (_playerController == null) {
			transform.position = Vector3.SmoothDamp(transform.position, targetPos - cameraOffset, ref _smoothDampVelocity, smoothDampTime);
			return;
		}

		if (_playerController.velocity.x > 0) {
			transform.position = Vector3.SmoothDamp(transform.position, targetPos - cameraOffset, ref _smoothDampVelocity, smoothDampTime);
		} else {
			Vector3 leftOffset = cameraOffset;
			leftOffset.x *= -1;
			transform.position = Vector3.SmoothDamp(transform.position, targetPos - leftOffset, ref _smoothDampVelocity, smoothDampTime);
		}

	}

	void LateUpdate() {
		UpdateCameraPosition();
	}
}
	
